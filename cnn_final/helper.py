import tensorflow as tf
import time
from tf_cnnvis import *

def deconvnet(graph, dic_val, tensor, total_iterations, num_iteration):
    layers = ["r", "p", "c"]
    total_time = 0
    start = time.time()
    is_success = deconv_visualization(graph_or_path = graph, value_feed_dict = dic_val,
                                      input_tensor=tensor, layers=layers, path_logdir="./log",
                                      path_outdir="./output")
    start = time.time() - start
    print("Total Time = %f" % (start))
    total_iterations += num_iteration

def max_pool_2x2(layer):
    return tf.nn.max_pool(value=layer, ksize=[1, 2, 2, 1], strides=[1, 2, 2, 1], padding='SAME')

def dropout(layer):
    return tf.nn.dropout(layer, keep_prob=0.5)

def relu(layer):
    return tf.nn.relu(layer)

def normalize(layer):
    return tf.contrib.layers.batch_norm(layer)

def flatten(layer):
    layer_shape = layer.get_shape()
    ## Number of features will be img_height * img_width* num_channels. But we shall calculate it in place of hard-coding it.
    num_features = layer_shape[1:4].num_elements()
    ## Now, we Flatten the layer so we shall have to reshape to num_features
    return tf.reshape(layer, [-1, num_features])

def dense(layer, num_outputs):
    num_inputs = layer.get_shape()[1:4].num_elements()
    #Let's define trainable weights and biases.
    weights = create_weights(shape=[num_inputs, num_outputs])
    biases = create_biases(num_outputs)
    # Fully connected layer takes input x and produces wx+b.Since, these are matrices, we use matmul function in Tensorflow
    return tf.matmul(layer, weights) + biases

def create_weights(shape):
    return tf.Variable(tf.truncated_normal(shape, stddev=0.05))

def create_biases(size):
    return tf.Variable(tf.constant(0.05, shape=[size]))

def conv2d(layer, num_input_channels, conv_filter_size, num_filters):
    ## We shall define the weights that will be trained using create_weights function.
    weights = create_weights(shape=[conv_filter_size, conv_filter_size, num_input_channels, num_filters])
    ## We create biases using the create_biases function. These are also trained.
    biases = create_biases(num_filters)
    ## Creating the convolutional layer
    layer = tf.nn.conv2d(input=layer, filter=weights, strides=[1, 1, 1, 1], padding='SAME')
    layer += biases
    return layer
