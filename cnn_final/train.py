import dataset
import tensorflow as tf
import time
from datetime import timedelta
import math
import random
import numpy as np
import matplotlib.pyplot as plt
#Adding Seed so that random initialization is consistent
from numpy.random import seed
seed(1)
from tensorflow import set_random_seed
set_random_seed(2)
import helper

BATCH_SIZE = 8

#Prepare input data
CLASSES = ['goat','neutral']
NUM_CLASSES = len(CLASSES)

# 20% of the data will automatically be used for validation
VALIDATION_SIZE = 0.1
IMG_SIZE = 128
CHANNELS = 3
TRAIN_PATH='data'

# We shall load all the training and validation images and labels into memory using openCV and use that during training
data = dataset.read_train_sets(TRAIN_PATH, IMG_SIZE, CLASSES, VALIDATION_SIZE)

print("Complete reading input data. Will Now print a snippet of it")
print("Number of files in Training-set:\t\t{}".format(len(data.train.labels)))
print("Number of files in Validation-set:\t{}".format(len(data.valid.labels)))


session = tf.Session()
tensor = tf.placeholder(tf.float32, shape=[None, IMG_SIZE,IMG_SIZE,CHANNELS], name='tensor')

## labels
y_true = tf.placeholder(tf.float32, shape=[None, NUM_CLASSES], name='y_true')
y_true_cls = tf.argmax(y_true, dimension=1)


##Network graph params
FILTER_SIZE_CONV1 = 3
NUM_FILTERS_CONV1 = 32

FILTER_SIZE_CONV2 = 3
NUM_FILTERS_CONV2 = 64

FILTER_SIZE_CONV3 = 3
NUM_FILTERS_CONV3 = 128

DENSE_SIZE = 128


conv1 = helper.conv2d(tensor, CHANNELS, FILTER_SIZE_CONV1, NUM_FILTERS_CONV1)
relu1 = helper.relu(conv1)
pool1 = helper.max_pool_2x2(relu1)
norm1 = helper.normalize(pool1)

conv2 = helper.conv2d(norm1, NUM_FILTERS_CONV1, FILTER_SIZE_CONV2, NUM_FILTERS_CONV2)
relu2 = helper.relu(conv2)
norm2 = helper.normalize(relu2)
pool2 = helper.max_pool_2x2(norm2)

conv3 = helper.conv2d(norm2, NUM_FILTERS_CONV2, FILTER_SIZE_CONV3, NUM_FILTERS_CONV3)
relu3 = helper.relu(conv3)
norm3 = helper.normalize(relu3)
pool3 = helper.max_pool_2x2(norm3)

flatten = helper.flatten(pool2)
fc1 = helper.dense(flatten, DENSE_SIZE)
dropout1 = helper.dropout(fc1)
fc2 = helper.dense(dropout1, NUM_CLASSES)

y_pred = tf.nn.softmax(fc2, name='y_pred')
y_pred_cls = tf.argmax(y_pred, dimension=1)

session.run(tf.global_variables_initializer())

cross_entropy = tf.nn.sigmoid_cross_entropy_with_logits(logits=fc2, labels=y_true)
cost = tf.reduce_mean(cross_entropy)
optimizer = tf.train.AdamOptimizer(learning_rate=1e-4).minimize(cost)
correct_prediction = tf.equal(y_pred_cls, y_true_cls)
accuracy = tf.reduce_mean(tf.cast(correct_prediction, tf.float32))

session.run(tf.global_variables_initializer())


def show_progress(epoch, feed_dict_train, feed_dict_validate, val_loss):
    acc = session.run(accuracy, feed_dict=feed_dict_train)
    val_acc = session.run(accuracy, feed_dict=feed_dict_validate)
    msg = "Training Epoch {0} --- Training Accuracy: {1:>6.1%}, Validation Accuracy: {2:>6.1%},  Validation Loss: {3:.3f}"
    print(msg.format(epoch + 1, acc, val_acc, val_loss))

saver = tf.train.Saver()

total_iterations = 0

def train(num_iteration):
    global total_iterations

    for i in range(total_iterations,
                   total_iterations + num_iteration):

        x_batch, y_true_batch, _, cls_batch = data.train.next_batch(BATCH_SIZE)
        x_valid_batch, y_valid_batch, _, valid_cls_batch = data.valid.next_batch(BATCH_SIZE)


        feed_dict_tr = {tensor: x_batch,
                           y_true: y_true_batch}
        feed_dict_val = {tensor: x_valid_batch,
                              y_true: y_valid_batch}

        session.run(optimizer, feed_dict=feed_dict_tr)


        if i % int(data.train.num_examples/BATCH_SIZE) == 0:
            val_loss = session.run(cost, feed_dict=feed_dict_val)
            epoch = int(i / int(data.train.num_examples/BATCH_SIZE))

            show_progress(epoch, feed_dict_tr, feed_dict_val, val_loss)

            saver.save(session, './model/model')

    file_writer = tf.summary.FileWriter('./summary', session.graph)

    # deconv visualization
    helper.deconvnet(tf.get_default_graph(), feed_dict_val, tensor, total_iterations, num_iteration)


train(num_iteration=20000)
